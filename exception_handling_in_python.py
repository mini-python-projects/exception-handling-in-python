#Write a script that reads a number and raise ValueError exception if the number is not between 0 and 100.

user_num = int(input('enter your number : '))

if not((user_num > 0) and (user_num < 100)):
    print('inside if')
    raise ValueError('input must be between 0 to 100')


#Write a script that reads a string and raise ValueError exception if the string has more than 10 characters.

user_str = input('enter your name : ')

if len(user_str) > 10:
    raise ValueError('length of name must be less than 10')


#Write a script that handles opening a non-existing file and prints the message File not found to the user.

try:
    input_file = open('dummy.txt','r')
except IOError:
    print('file not found')
else:
    print('file found & open')
finally:
    print('file opening para executed')


#Define a class Circle, whose objects are initialized with radius as it's attribute.
#Ensure that the object creation raises RadiusInputError, a user defined exception, when the input radius is not a number.

class RadiusInputError(Exception):
    pass

class Circle(RadiusInputError):
    def __init__(self,r):
        self.r = r
        if not str(self.r).isnumeric():
            raise RadiusInputError('radius must be numeric')

c1 = Circle(10)
c2 = Circle('e')
            
    
